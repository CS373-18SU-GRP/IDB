import expect from 'expect';
import Navbar from './../src/Components/Navbar';
import React from 'react';
import { shallow } from 'enzyme';

describe("Tests Navbar.js", () => {
  it("Renders without exploding", () => {
    const wrapper = shallow(<Navbar />);  
    expect(wrapper.length).toBe(1);
  });

  it("Contains at least 6 links", () => {
    const wrapper = shallow(<Navbar.WrappedComponent location={{pathname: ''}}/>);
    expect(wrapper.find('Link').length).toBeGreaterThanOrEqual(6);
  })
});
