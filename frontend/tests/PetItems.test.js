import expect from 'expect';
import PetItems from './../src/Components/Model/Pets/PetItems';
import React from 'react';
import { shallow } from 'enzyme';

describe("Tests PetItems.js", () => {

  const minProps = {
    petData:
        {
            id: "2",
            name: "Austin Wildlife Rescue",
            image: "https://s3-media3.fl.yelpcdn.com/bphoto/yrmDh-ueiSju9B5JwY64fg/ls.jpg",
            location: "Austin",
            rating: "4.5",
            contact: "(512) 472-9453"
        }
  }

  test("Renders without exploding", () => {

    const wrapper = shallow(<PetItems {...minProps}/>);

    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
  });

  test("Two links to its entity page", () => {

    const wrapper = shallow(<PetItems {...minProps}/>);
    expect(wrapper.find({ to: '/Pets/PetEntity/'.concat(minProps.petData.id) }).length).toEqual(2);
  });

});
