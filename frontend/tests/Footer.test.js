import expect from 'expect';
import Footer from './../src/Components/Footer';
import React from 'react';
import { shallow } from 'enzyme';

describe("Tests Footer.js", () => {
  it("Renders without exploding", () => {
    const wrapper = shallow(<Footer />);  
    expect(wrapper.length).toBe(1);
  });

  it("Contains link to top", () => {
    const wrapper = shallow(<Footer />);
    expect(wrapper.find('Link').length).toBe(1);
  })
});
