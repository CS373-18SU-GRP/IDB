import expect from 'expect';
import {PetEntity} from './../src/Pages/Entities/PetEntity';
import React from 'react';
import { shallow } from 'enzyme';

describe("Tests PetEntity.js", () => {

  let mockFetch = jest.fn();
  const minProps = {
    petInfo: {
      age: "Adult",
      animal: "Dog",
      breed: "Chihuahua",
      description: "Charlie is an adorable, feisty and playful, three-year-old, male, fawn and white Chihuahua looking for a loving guardian.↵A ↵Charlie is extremely playful and loves to play fetch - even with a huge tennis ball almost as big as his little head! He will also dance on his hind legs for a treat. Little Charlie has a lot to learn in life and heas looking for someone to give him a chance. Heas fearful of new people and other dogs at first, and will nip when nervous.↵A ↵Charlie knows a bunch of commands and is perfectly housebroken and crate-trained. He bonds very closely to one person and is looking for an experienced dog-lover to keep him safe and boost his confidence.A ↵A ↵Charlie is very healthy, neutered, up-to-date on vaccines, micro-chipped, de-wormed, heartworm-tested and on monthly preventative. If you are interested in meeting, fostering, or adopting Charlie, please contact Gloria at 773-907-0305 or fido@famousfidorescue.com . His adoption fee of $200 benefits the rescued animals of Famous Fido.↵A ↵A ",
      id: "23848501",
      image_url: "http://photos.petfinder.com/photos/pets/23848501/3/?bust=1453304869&width=500&-x.jpg",
      name: "Charlie",
      sex: "Male",
      shelter: "IL666",
      size: "S",
      vet: "eONTvMLYNkbwu7v5fLBLWA"
    },
    vetInfo: {
      address1: "",
      address2 : "",
      address3 : "",
      city : "Chicago",
      close_shelters : ["IL245", "IL27", "IL537", "IL608", "IL666", "IL751"],
      country : "US",
      display_address : "a",
      display_phone : "(312) 388-5150",
      id : "-GtQ2DCYD5VQ3IOoemDgew",
      image_url : "http : /s3-media2.fl.yelpcdn.com/bphoto/0z619pb-I4Y2KAAXQZTVDQ/o.jpg",
      latitude : 41.9944,
      longitude : -87.6642,
      name : "Cozy Cats and Daily Dogs",
      pets : [],
      phone : "+13123885150",
      rating : 5,
      review_count : 88,
      state : "IL",
      zip_code : "60660",
    },
    shelterInfo: {
      address1: null, 
      address2: null, 
      city: "Lombard", 
      close_vets: [],
      email: "animalheartline@hotmail.com",
      id: "IL115",
      latitude: 41.8785,
      longitude: -88.0124,
      name: "Animal Heartline Humane Association",
      pets: ["40734569", "41352679", "41428034", "42098125", "42136732"],
      phone: "630-341-3411",
      state: "IL",
      zip: "60148",
    },
    fetchPetEntity: {mockFetch},
    match:{params: {petId:"123"}}
  }

  test("Renders without exploding", () => {
    let mockFetch = jest.fn();
    const wrapper = shallow(<PetEntity {...minProps} fetchPetEntity={mockFetch}/>);
    
    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
    expect(mockFetch).toHaveBeenCalled();
  });

});
