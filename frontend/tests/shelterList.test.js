import expect from 'expect';
import shelterList from './../src/Pages/ListingPage/ShelterList/shelterList';
import React from 'react';
import { shallow, mount, render } from 'enzyme';

describe("Tests shelterList.js", () => {

  test("Renders without exploding", () => {

    const wrapper = shallow(<shelterList />);

    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
  });

  const minProps = {
    shelters: [
        {
            id: "2",
            name: "Austin Wildlife Rescue",
            image: "https://s3-media3.fl.yelpcdn.com/bphoto/yrmDh-ueiSju9B5JwY64fg/ls.jpg",
            location: "Austin",
            rating: "4.5",
            contact: "(512) 472-9453"
        },
        {
            id: "3",
            name: "Austin Humane Society",
            image: "https://s3-media1.fl.yelpcdn.com/bphoto/uM9O_kuOfIF2EcAiyFMuZA/o.jpg",
            location: "Austin",
            rating: "4.0",
            contact: "(512) 646-7387"
        },
        {
            id: "0",
            name: "Austin Dog Rescue",
            image: "https://s3-media4.fl.yelpcdn.com/bphoto/oDPwa3LvjhjEaur87Dunrw/o.jpg",
            location: "Austin",
            rating: "4.5",
            contact: "(512) 827-9787"
        },
        {
            id: "1",
            name: "Kate To The Rescue Animal Rescue",
            image: "https://s3-media2.fl.yelpcdn.com/bphoto/XLw3zMOZ7TIcXxW5jsIBNg/o.jpg",
            location: "Austin",
            rating: "0",
            contact: "katetotherescue.org"
        }
    ],
  }

  test("Takes shelters as props", () => {
    const wrapper = shallow(<shelterList {...minProps}/>);

    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
  });
});
