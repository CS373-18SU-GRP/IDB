import expect from 'expect';
import RatingStar from './../src/Components/RatingStar';
import React from 'react';
import { shallow } from 'enzyme';

describe("Tests RatingStar.js", () => {
  it("Renders without exploding", () => {
    const wrapper = shallow(<RatingStar />);  
    expect(wrapper.length).toBe(1);
  });

  it("Renders four checked stars", () => {
    const wrapper = shallow(<RatingStar rating={4}/>);  
    expect(wrapper.find('.checked').length).toBe(4);
  })

  it("Renders zero checked stars", () => {
    const wrapper = shallow(<RatingStar rating={0}/>);  
    expect(wrapper.find('.checked').length).toBe(0);
  })
});
