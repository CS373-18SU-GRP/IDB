import React, { Component } from 'react';
import fetchData from './../Assets/StaticData';
import Issue from './Issue';
import './../CSS/Developer.css';

export default class Developer extends Component {
  constructor(props) {
    super(props);
    this.state= {
      devInfo: [],
      count: 0,
    }
  }


  /*
  Merges contributor information from gitlab with static information (like username, bio, picture, etc)
  also increments total commits/unit tests
  */
  componentWillMount() {
    fetch ('https://gitlab.com/api/v4/projects/7164816/repository/contributors', {
    }).then(res => res.json())
    .then(jsonArray => {
      var pruned = [];
      for(var i in jsonArray)
      {
        let developerInfo = fetchData(jsonArray[i].email);
        if (developerInfo === 'missing')
          continue;
        jsonArray[i].name = developerInfo.name;
        jsonArray[i].bio = developerInfo.bio;
        jsonArray[i].image = developerInfo.image;
        jsonArray[i].userId = developerInfo.userId;
        jsonArray[i].unitTests = developerInfo.unitTests;
        this.props.incrementCommits(jsonArray[i].commits);
        this.props.incrementUnit(jsonArray[i].unitTests);
        pruned.push(jsonArray[i]);
      }
      return pruned;
    })
    .then(data => this.setState({devInfo: data}));
  }


  render(){
    return this.state.devInfo.map(dev => (
      <div className="col-md-4 d-flex align-items-stretch" key={dev.name}>
        <div className="card mb-4 box-shadow">
          <div className="card-body">
            <div className="dev">
              <img className="card-img-top" src={dev.image} alt="Card cap"/>
              <h5 className="card-title">{dev.name}</h5>
            </div>
            <p className="card-text">{dev.bio}</p>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">Commits: {dev.commits}</li>
            <Issue user={ dev.userId } increment= { this.props.incrementIssues.bind(this) }/>
            <li className="list-group-item">Unit Tests: {dev.unitTests}</li>
          </ul>  
        </div>
      </div>
      )
    )
  }
}
