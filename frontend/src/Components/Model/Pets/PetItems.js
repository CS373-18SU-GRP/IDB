import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../CSS/PetItems.css';


export default class PetItems extends Component {
  render() {
    let info = this.props.petData.id;
    return (
    <div className="col-md-4">
        <div className="card mb-4 box-shadow">
          <div className="petimg">
            <Link to={"/Pets/PetEntity/".concat(info)}><img className="card-img-top" src={this.props.petData.image_url} onError={(e)=>{e.target.src="https://www.scandichotels.se/Static/img/placeholders/image-placeholder_3x2.svg"}} alt=""/></Link>
          </div>
          <div className="card-body">
          <p className="card-text"><Link  to={"/Pets/PetEntity/".concat(info)}>{this.props.petData.name}</Link></p>
            <div className="d-flex justify-content-between traits">
              <h6 id="badgeList">
                <span className="badge badge-pill badge-light">{this.props.petData.age}</span>
                <span className="badge badge-pill badge-light">{this.props.petData.breed}</span>
                <span className="badge badge-pill badge-light">{this.props.petData.sex}</span>
              </h6>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
