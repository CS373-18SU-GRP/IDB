import React, { Component } from 'react'

export default class RatingStar extends Component {
  render() {
    let color={
      color: 'orange'
    };

    let stars = [];
    if (this.props.rating !== 0){
      for (let i = 0; i < 5; i++){
        if(i < this.props.rating){
          stars.push(
            <span className="fa fa-star checked" style={color} key={i}></span>
          );
        }
        else{
          stars.push(
            <span className="fa fa-star" key={i}></span>
          );
        }
      }
    }

    return (
    <div className="d-flex">
      {stars}
    </div>
    )
  }
}
