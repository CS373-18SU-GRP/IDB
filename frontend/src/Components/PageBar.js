import React, { Component } from 'react';
import { Link } from 'react-router-dom';

let range = (start, stop) => {
  let i = start;
  let range = [];

  while (i < stop) {
    range.push(i++);
  }
  return range;
}

export default class PageBar extends Component{
  constructor(props) {
    super(props);
    this.state = {
      queryString : "",
    }
  }

  componentWillMount() {
    if (this.props.query !== undefined)
      this.setState({queryString : this.props.query});
    else
      this.setState({queryString : ""});
  }

  componentDidUpdate(prevProps) {
    if (prevProps.query !== this.props.query) {
      if (this.props.query !== undefined)
        this.setState({queryString : this.props.query});
      else
        this.setState({queryString : ""});
    }
  }
  render() {
    const first = 1;
    const domain = this.props.domain;
    let queryString = this.state.queryString;
    const pageNumber = parseInt(this.props.pageNum, 10);
    const last = this.props.totalPages;
    const left = Math.max(first, pageNumber - 2);
    const right = Math.min(last + 1, pageNumber + 3);

    /*
    creates buttons to display left of current page button
    */
    let leftButtons = range(left, pageNumber).map(num => {
      return (
        <li className="page-item" key={num}>
          <Link to={domain.concat(num, queryString)} className="page-link">{num}</Link> 
        </li>
      )
    });

    /*
    creates buttons to display right of current page button
    */
    let rightButtons = range(pageNumber + 1, right).map(num => {
      return (
        <li className="page-item" key={num}> 
          <Link to={domain.concat(num, queryString)} className="page-link">{num}</Link> 
        </li>
      )
    });

    /*
    determines whether or not to create a ... and last page button to let user know how many pages are left
    */
    let lastPage = [
      (last - pageNumber) <= 3 ? null : 
        <li className="page-item" key="secondToLast">
          <div className="page-link disabled">...</div>
        </li>,
        <li className="page-item" key="last">
          <Link to={domain.concat(last, queryString)} className="page-link">{last}</Link>
        </li>
    ]

    return (
      <nav id="pagebar">
        <ul className="pagination justify-content-center">
          <li className={pageNumber === first ? "page-item disabled" : "page-item"}>
            <Link to={domain.concat(first, queryString)} className="page-link">&laquo;</Link>
          </li>
          {leftButtons}
          <li className="page-item active">
            <div className="page-link currentPage disabled">{pageNumber} </div>
          </li>
          {rightButtons}
          {last - pageNumber > 2 ? lastPage : null}
          <li className={(pageNumber === last || last === 0) ? "page-item disabled" : "page-item"}>
            <Link to={domain.concat(last, queryString)} className="page-link">&raquo;</Link>
          </li>
        </ul>
      </nav>
    )
  }
}