import {
  FacebookShareButton, 
  FacebookIcon, 
  TwitterShareButton, 
  TwitterIcon, 
  RedditShareButton, 
  RedditIcon
} from 'react-share';
import React, { Component } from 'react';

export default class Share extends Component {
  constructor(props) {
    super(props);
    this.url = "http://www.connectpetsto.me/";
    switch(this.props.type) {
      case 'vet': this.url = this.url.concat("Vets/VetEntity/"); break;
      case 'pet': this.url = this.url.concat("Pets/PetEntity/"); break;
      case 'shelter': this.url = this.url.concat("Shelters/ShelterEntity/"); break;
      default: break;
    }
  }

  render() {
    return (
      <table className="share-buttons">
        <tbody>
          <tr>
            <td>
              <FacebookShareButton className="button"  quote="lol" url={this.url + this.props.id} >
                <FacebookIcon size={32} round={true}/>
              </FacebookShareButton>
            </td>
            
            <td>
              <TwitterShareButton className="button" url={this.url + this.props.id}>
                <TwitterIcon size={32} round={true}/>
              </TwitterShareButton>
            </td>

            <td>
              <RedditShareButton className="button" url={this.url + this.props.id}>
                <RedditIcon size={32} round={true}/>
              </RedditShareButton>
              </td>
          </tr>
        </tbody>
      </table>
    )
  }
}