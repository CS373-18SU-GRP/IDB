import davidimg from './david.jpg'
import ajimg from './aj.jpg'
import wesleyimg from './wesley.png'
import aleximg from './alex.jpg'
import jeffreyimg from './jeffrey.png'
import yoshiimg from './yoshi.jpg'

export default function fetchData(name)
{
  let temp = {};

  let david =
  {
      "name": "David Mao",
      "bio": "Responsible for frontend. Student at UT. Loves both dogs and cats but not buildings.",
      "image": davidimg,
      "userId": '2440012',
      "unitTests": 14,
  };

  let yoshi = 
  {
    "name": "Yoshinobu Nakada",
    "bio": "Responsible for frontend and deployment. Student at UT, soon to be graduate of UT.",
    "image": yoshiimg,
    "userId": '2458894',
    "unitTests": 13,
  };

  let aj = 
  {
    "name": "AJ Soud",
    "bio": "Responsible for backend, deployment. Former combat medic, current UT student.",
    "image": ajimg,
    "userId": '2431421',
    "unitTests": 99,
  };

  let alex = 
  {
    "name": "Alex Flores Escarcega",
    "bio": "Rising senior at UT. Responsible for frontend. I love coding and I love buildings, we should learn how to code buildings!",
    "image": aleximg,
    "userId": '2480227',
    "unitTests": 30,
  };

  let jeffrey = 
  {
    "name": "Jeffrey Zhu",
    "bio": "Responsible for backend. Junior at UT. Loves cats, but doesn't own one.",
    "image": jeffreyimg,
    "userId": '2472072',
    "unitTests": 22,
  };

  let wesley = 
  {
    "name": "W. Wesley Monroe",
    "bio": "Responsible for design and backend. Loves biking, bluegrass, and herding cats.",
    "image": wesleyimg,
    "userId": '2438307',
    "unitTests": 24,
  }

  switch (name)
  {
    case 'david.mao.92@gmail.com':
      temp = david; break;
    case 'vivayoshivitss1333@gmail.com':
      temp = yoshi; break;
    case 'soud@utexas.edu':
      temp = aj; break;
    case 'jeffzhu123@utexas.edu':
      temp = jeffrey; break;
    case 'alex.floresescar@gmail.com':
      temp = alex; break;
    case 'wmonroe@cs.utexas.edu':
      temp = wesley; break;
    default:
      temp = 'missing';
  }

  return temp;
}

export const tools_json = [
  {
    "name": "React.JS",
    "description": "Javascript library used to create UI components.",
  },
  {
    "name": "Redux",
    "description": "State manager used for ease of communicating between UI components.",
  },
  {
    "name": "React Router",
    "description": "React extension used to manage application states.",
  },
  {
    "name": "Bootstrap",
    "description": "CSS framework used to style web pages.",
  },
  {
    "name": "Postman",
    "description": "API design tool used to create our website API.",
  },
  {
    "name": "Selenium",
    "description": "End to end tester used to test our GUI.",
  },
  {
    "name": "Jest",
    "description": "Unit test runner used to run our tests.",
  },
  {
    "name": "Enzyme",
    "description": "Unit testing utility used to wrap React Components and call measuring functions on them.",
  },
  {
    "name": "Docker",
    "description": "Container management platform used to create a portable sandbox for development.",
  },
  {
    "name": "Flask",
    "description": "Microframework for building web applications and APIs.",
  },
  {
    "name": "Flask-SQLAlchemy",
    "description": "Object Relational Mapper used to construct our database.",
  },
  {
    "name": "Flask-RESTful",
    "description": "API resource used to manage API routing."
  },
  {
    "name": "Marshmallow",
    "description": "Serializer used to convert SQLAlchemy objects to JSON format.",
  },
  {
    "name": "Requests",
    "description": "Python library for simplifying HTTP requests.",
  },
  {
    "name": "MySQL and related libraries",
    "description": "Database tools used to connect our SQLAlchemy objects to our SQL database."
  },
  {
    "name": "ElasticSearch",
    "description": "Full-text search engine used to handle search requests to our backend."
  }
];    

export const states = [ 
  "AK",
  "AL",
  "AR",
  "AZ",
  "CA",
  "CO",
  "CT",
  "DE",
  "FL",
  "GA",
  "HI",
  "IA",
  "ID",
  "IL",
  "IN",
  "KS",
  "KY",
  "LA",
  "MA",
  "MD",
  "ME",
  "MI",
  "MN",
  "MO",
  "MS",
  "MT",
  "NC",
  "ND",
  "NE",
  "NH",
  "NJ",
  "NM",
  "NV",
  "NY",
  "OH",
  "OK",
  "OR",
  "PA",
  "RI",
  "SC",
  "SD",
  "TN",
  "TX",
  "UT",
  "VA",
  "VT",
  "WA",
  "WI",
  "WV",
  "WY"
];

export const animals = [
  "Barnyard",
  "Bird",
  "Cat",
  "Dog",
  "Horse",
  "Rabbit",
  "Small & Furry",
  "Scales, Fins & Other",
]

export const sortAnimals = [
  "Name",
  "Size",
]

export const sortShelters = [
  "Name",
]

export const sortVets = [
  "Name",
  "Rating",
]

export const sizes = [
  "S",
  "M",
  "L",
  "XL",
]

export const sexes = [
  "M",
  "F",
  "U",
]

export const ages = [
  "Baby",
  "Young",
  "Adult",
  "Senior",
]

export const ratings = [
  "5",
  "4",
  "3",
  "2",
  "1",
]

export const mapToDisplay = {
  'AL' : 'Alabama',
  'AK' : 'Alaska',
  'AZ' : 'Arizona',
  'AR' : 'Arkansas',
  'CA' : 'California',
  'CO' : 'Colorado',
  'CT' : 'Connecticut',
  'DE' : 'Delaware',
  'FL' : 'Florida',
  'GA' : 'Georgia',
  'HI' : 'Hawaii',
  'ID' : 'Idaho',
  'IL' : 'Illinois',
  'IN' : 'Indiana',
  'IA' : 'Iowa',
  'KS' : 'Kansas',
  'KY' : 'Kentucky',
  'LA' : 'Louisiana',
  'ME' : 'Maine',
  'MD' : 'Maryland',
  'MA' : 'Massachusetts',
  'MI' : 'Michigan',
  'MN' : 'Minnesota',
  'MS' : 'Mississippi',
  'MO' : 'Missouri',
  'MT' : 'Montana',
  'NE' : 'Nebraska',
  'NV' : 'Nevada',
  'NH' : 'New Hampshire',
  'NJ' : 'New Jersey',
  'NM' : 'New Mexico',
  'NY' : 'New York',
  'NC' : 'North Carolina',
  'ND' : 'North Dakota',
  'OH' : 'Ohio',
  'OK' : 'Oklahoma',
  'OR' : 'Oregon',
  'PA' : 'Pennsylvania',
  'RI' : 'Rhode Island',
  'SC' : 'South Carolina',
  'SD' : 'South Dakota',
  'TN' : 'Tennessee',
  'TX' : 'Texas',
  'UT' : 'Utah',
  'VA' : 'Virginia',
  'VT' : 'Vermont',
  'WA' : 'Washington',
  'WV' : 'West Virginia',
  'WI' : 'Wisconsin',
  'WY' : 'Wyoming',
  'M' : 'Male',
  'F' : 'Female',
  'U' : 'Unidentified',
  '5' : '5',
  '4' : '4+',
  '3' : '3+',
  '2' : '2+',
  '1' : '1+',
  }
export const mapSizeToDisplay = {
  'S' : 'Small',
  'M' : 'Medium',
  'L' : 'Large',
  'XL' : 'Extra Large', 
}