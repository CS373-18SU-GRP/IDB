import { FETCH_VET_ENTRY } from './../Actions/type';

const initialState = {
    items: [],
    item: {}
};

export default function(state=initialState, action){
    switch(action.type){
        case FETCH_VET_ENTRY:
            return{
                ...state,
                items: action.data.vet
            };
        default:
            return state;
    }
}
