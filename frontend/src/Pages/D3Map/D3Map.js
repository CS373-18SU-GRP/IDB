import React, {Component} from 'react';
import D3Carousel from './D3Carousel'
import './../../CSS/Carousel.css';

class Home extends Component{
  render(){
    return (
    <div className="listingPage">
      <D3Carousel/>
    </div>
    )
  }
}

export default Home
