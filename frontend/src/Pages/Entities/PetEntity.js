import React, { Component } from 'react';
import './../../CSS/PetEntity.css';
import { connect } from 'react-redux';
import { fetchPetEntity, fetchShelterEntity, fetchVetEntity } from './../../Actions/dataModelAction';
import ShelterCard from './Cards/ShelterCard';
import VetCard from './Cards/VetCard';
import Share from './../../Components/Share';
import Carousel from './Carousel/Carousel';
import axios from 'axios';
import BreedInfo from './BreedInfo';
import InteractiveMap from '../../Components/InteractiveMap';
import { HOST } from '../../Actions/type';

// parse breed string('shiba/Beagle') and make list
function parseBreedString(s){
    let b_list = [];
    while(true){
        let i = s.indexOf("/");
        if (i === -1){
            b_list.push(s.substring(0));
            break;
        }else{
            b_list.push(s.substring(0, i));
            s = s.substring(i + 1);
        }
    }
    return b_list;
}

export class PetEntity extends Component{
    constructor(props) {
        super(props);
        this.state= {
          'breeds_info': []
        }

        this.style = {
            backgroundColor : '#bcaaa4',
        }
      }

    componentWillMount(){
        this.props.fetchPetEntity(this.props.match.params.petId, 'pet');
    }

    componentDidUpdate(nextProps){
        if (this.props.petInfo.shelter !== null && !(this.props.shelterInfo.id === this.props.petInfo.shelter)){
            this.props.fetchShelterEntity(this.props.petInfo.shelter);
            
        }
        if (this.props.petInfo.vet !== null && !(this.props.vetInfo.id === this.props.petInfo.vet)){
            this.props.fetchVetEntity(this.props.petInfo.vet);
            
        }

        // get breed's temperament info from Breed table
        if (this.state.breeds_info.length === 0)
            for (let breed of parseBreedString(this.props.petInfo.breed)){
                const baseUrl = "http://"+HOST+":5000/api/breed"
                let url = baseUrl.concat("/", breed); 
                axios.get(url, {
                    headers: { 
                        'Access-Control-Allow-Origin' : '*',
                        'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',          
                    },
                    responseType: 'json',
                })
                .then(res => {
                    this.setState({breeds_info:[...this.state.breeds_info, res.data.breed]});
                });
            }
        
    }

    render(){
        
        // pet, vet, shelter info are avalilable
        // if you wanna know, how it store, simply call
        // console.log(info)
        let info = this.props.petInfo;
        let s_info = this.props.shelterInfo;
        let v_info = this.props.vetInfo;

        // set breeds temperament
        let breeds = []
        for (let it of this.state.breeds_info)
            breeds.push(<BreedInfo breed={it.id} temperament={it.temperament} detail={it.detail} key={it.id}/>)
        
        if (info.sex === 'M' || info.sex === 'Male')
            info.sex = 'Male';
        else
            info.sex = 'Female';

        // bottom cards for vet and shelter
        let entityAttribute = [];
        if(info.shelter !== null){
            entityAttribute.push(
                <ShelterCard info={s_info} key={1}/>
            );
        }
        if(info.vet !== null){
            entityAttribute.push(
                <VetCard info={v_info} key={2}/>
            );
        }
        
        return (
            <div className="petEntity listingPage">     
                
                <Carousel info={info} />
                <main role="main" className="container">
                    <div className="row">
                        <div className="col-md-8 blog-main">
                            <div className="row mb-2" id="noPad">
                                <div className="col-md-12">
                                    <div className="card flex-md-row mb-4 box-shadow">
                                        <div className="card-body d-flex flex-column align-items-start">
                                            <h2 className="d-inline-block mb-2 text-black"><b>{info.name}</b></h2>
                                            <p className="text-secondary">{info.breed} ・ {s_info.city} {s_info.state}</p>
                                            <br/>
                                            <h4 className="text-secondary"><b>Age</b></h4>
                                            <p>{info.age}</p>
                                            <h4 className="text-secondary">Sex</h4>
                                            <p>{info.sex}</p>
                                            <h4 className="text-secondary">Size</h4>
                                            <p>{info.size}</p>
                                            <h4 className="text-secondary">History</h4>
                                            <p>{info.description}</p>
                                        </div>
                                        
                                    </div>
                                </div>
                        
                            </div>

                        </div>

                        

                         <aside className="col-md-4 blog-sidebar" styles={this.style}>
                            <div className="p-3 mb-3 rounded">
                                <Share type='pet' id={this.props.match.params.petId} />
                                {s_info.name !== undefined ?
                                    <InteractiveMap latitude={s_info.latitude} longitude={s_info.longitude} name={s_info.name} size={130}/>
                                : null} 
                                
                                <div className="mb-0">Shelter: <p className="text-primary">{s_info.name}</p></div>
                                <div className="mb-0">Contact: <p className="text-primary">{s_info.phone}</p></div>
                                <div className="mb-0">Email: <p className="text-primary">{s_info.email}</p></div>
                                <div className="mb-0">Location: <p className="text-primary">{s_info.address1} {s_info.city} {s_info.state} {s_info.zip}</p></div>
                            </div>

                            <div className="p-3">
                                <h4 className="font-italic">Breed Detail</h4>
                                {breeds}
                            </div>
                        </aside> 

                    </div>

                </main>
                <div className="row" id="noPad">
                    {entityAttribute}
                </div>
            </div>


        );
    }
}

const mapStateToProps = state => ({
    petInfo: state.petInfo.items,
    vetInfo: state.vetInfo.items,
    shelterInfo: state.shelterInfo.items,
})

export default connect(mapStateToProps, {fetchPetEntity, fetchShelterEntity, fetchVetEntity})(PetEntity)
