import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class VetCard extends Component {
  render() {
    // <RatingStar/>
    return (
        <div className="col-md-6">
          <div className="card flex-md-row mb-4 box-shadow h-md-250">
              <div className="card-body d-flex flex-column align-items-start">
              <strong className="d-inline-block mb-2 text-success">Vet</strong>
              <h5 className="mb-0">
                  <Link className="text-dark" to={"/Vets/VetEntity/".concat(this.props.info.id)}>{this.props.info.name}</Link>
              </h5>
              <p className="card-text mb-auto">{this.props.info.city} {this.props.info.state}</p>
              <p className="card-text mb-auto">Contact: {this.props.info.display_phone}</p>

              <Link className="text-dark" to={"/Vets/VetEntity/".concat(this.props.info.id)}><p className="text-primary">More Detail</p></Link>
              </div>
              <img className="card-img-right flex-auto d-none d-lg-block" src={this.props.info.image_url} alt=""/>
          </div>
          </div>
        
    )
  }
}
