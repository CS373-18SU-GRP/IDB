import React, {Component} from 'react';
import {withRouter, Link} from 'react-router-dom';
import queryString from 'query-string';

class SearchHeader extends Component {
  constructor(props) {
    super(props);

    this.style = {
      backgroundColor: "#efebe9",
    }

    this.state = {
      q: "",
    }
    this.searchDomain = "";
    switch(this.props.domain) {
      case 'Pets': this.searchDomain = '/SearchPets/1?'; break;
      case 'Vets': this.searchDomain = '/SearchVets/1?'; break;
      case 'Shelters': this.searchDomain = '/SearchShelters/1?'; break;
      default: this.searchDomain='/Search/1?';
    }
  }

  searchUpdate(event){
    this.setState({q: event.target.value});
  }

  submitSearch(event) {
    if (event.key === 'Enter'){
      this.props.history.push(this.searchDomain.concat(queryString.stringify(this.state)));
      this.props.updateParent();
    }
  }

  componentWillMount() {
    if (this.props.location.search !== undefined)
      this.setState({q: queryString.parse(this.props.location.search).q});
  }

  render() {
    return (
      <nav className="navbar navbar-expand-lg col-md-9" style={this.style}>
        <label htmlFor="search-input" className="col-form-label">{'Search '.concat(this.props.domain)}</label>
          <div className="col">
            <input className="form-control" type="search" value={this.state.q} onChange={this.searchUpdate.bind(this)} id="search-input" onKeyPress={this.submitSearch.bind(this)}/>
          </div>
          <Link to={this.searchDomain.concat(queryString.stringify(this.state))}>
              <button className="btn" type="submit">Search</button>
          </Link>
      </nav>
    )
  }

}

export default withRouter(SearchHeader);