import React, { Component } from 'react';
import SearchResult from './SearchResult';
import axios from 'axios';
import PageBar from '../../Components/PageBar';
import SearchHeader from './SearchHeader';
import { HOST } from '../../Actions/type';


export default class SearchResultsPage extends Component { 
    constructor(props){
        super(props);
        this.state = {
            petData:[],
            vetData:[],
            shelterData:[],
            petTotalItems: 1,
            vetTotalItems: 1,
            shelterTotalItems: 1,
            q: this.props.location.search
        };
    }

    componentDidUpdate(prevProps, prevState){
        // Update state if change
        if (this.props.match.params.pageNum !== prevProps.match.params.pageNum || this.state.q !== prevState.q || prevProps.location.search !== this.props.location.search){
            if (prevProps.location.search !== this.props.location.search)
                this.setState({q: this.props.location.search});
            this.getPets();
            this.getVets();
            this.getShelters();
        }
    }

    componentWillMount(){
        this.getPets();
        this.getVets();
        this.getShelters();
    }


      searchUpdate(event){
        this.forceUpdate();
      }

    getPets(){
        // Grab pet search results
        let url = "http://"+HOST+":5000/api/pet/search".concat(this.props.location.search).concat("&page=").concat(this.props.match.params.pageNum).concat("&size=3");
        axios.get(
            url, {
              headers: { 
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',          
              },
              responseType: 'json',
          }).then(res =>(this.setState({petData: res.data["hits"], petTotalItems: res.data.total})));
    }

    getVets(){
        // Grab vet search results
        let url = "http://"+HOST+":5000/api/vet/search".concat(this.props.location.search).concat("&page=").concat(this.props.match.params.pageNum).concat("&size=3");
        axios.get(
            url, {
              headers: { 
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',          
              },
              responseType: 'json',
          }).then(res =>(this.setState({vetData: res.data["hits"], vetTotalItems: res.data.total})));
    }

    getShelters(){
        // Grab shelter search results
        let url = "http://"+HOST+":5000/api/shelter/search".concat(this.props.location.search).concat("&page=").concat(this.props.match.params.pageNum).concat("&size=3");
        axios.get(
            url, {
              headers: { 
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',          
              },
              responseType: 'json',
          }).then(res =>(this.setState({shelterData: res.data["hits"], shelterTotalItems: res.data.total})));
    }

    render(){
        // Populate data from API call to respective search card component
        let petItems = this.state.petData.map(pet =>
            <SearchResult url="/Pets/PetEntity/" info={pet} key={pet._id}/>
        );
        let vetItems = this.state.vetData.map(vet => 
            <SearchResult url="/Vets/VetEntity/" info={vet} key={vet._id}/>
        );
        let shelterItems = this.state.shelterData.map(shelter => 
            <SearchResult url="/Shelters/ShelterEntity/" info={shelter} key={shelter._id}/>
        );

        // Total items helps us know the minimum amount of pages needed for pagination on results pages
        let totalItems = Math.max(this.state.petTotalItems, this.state.vetTotalItems, this.state.shelterTotalItems);

        return(
            <div>
                <div className="album py-5 bg-light listingPage">
                    <div className="container fillPage">
                        <SearchHeader domain="" updateParent={this.searchUpdate.bind(this)}/>
                        <br />

                        <h3> Pets </h3>
                        {petItems.length === 0 ? 'No Pets to Display' : petItems}
                        <h3> Vets </h3>
                        {vetItems.length === 0 ? 'No Vets to Display' : vetItems}
                        <h3> Shelters </h3>
                        {shelterItems.length === 0 ? 'No Shelters to Display' : shelterItems}
                    </div>
                <br />
                {/* Number of pages to make available for pagination is roughly max number of individual items divided by 3 */}
                <PageBar domain="/Search/" pageNum={this.props.match.params.pageNum} totalPages={Math.ceil(totalItems / 3)} query={this.props.location.search} />
                </div>
            </div>
        );
    }
}
