import React, { Component } from 'react';
import VetItems from './../../../Components/Model/Vets/VetItems';
import PageHeader, {HeaderItem} from './../../../Components/PageHeader'
import PageBar from './../../../Components/PageBar';
import { fetchData } from './../../../Actions/dataModelAction'
import {connect} from 'react-redux';

export class vetList extends Component {
    constructor(props) {
        super(props);
        this.state = {
          "current_page": 1,
        };
    }
      
      // get total number of pages
      // fetchData also gets vets for this page of results
    componentWillMount() {
        this.props.fetchData('vet', this.props.match.params.pageNum, this.props.location.search);
        this.setState({current_page: this.props.match.params.pageNum});
    }
    // check if page number has changed, or query string has changed
    componentDidUpdate(prevProps){
        if (prevProps.match.params.pageNum !== this.props.match.params.pageNum || prevProps.location.search !== this.props.location.search){
            this.setState({current_page: this.props.match.params.pageNum});
            this.props.fetchData('vet', this.props.match.params.pageNum, this.props.location.search); 
        }
    }

    render() {
        let pageNum = this.props.match.params.pageNum;
        let vetItems = this.props.vets.map(vet => {
            return(
                <VetItems vetData = {vet} key={vet.id}/>
            );
        });
        return (
            <div className="album py-5 bg-light listingPage">
                <div className="container fillPage">
                    <PageHeader heading="Vets" domain="/FindVets/" query={this.props.location.search}>
                        <HeaderItem domain="/FindVets/" query={this.props.location.search} type="state"/>
                        <HeaderItem domain="/FindVets/" query={this.props.location.search} type="city"/>
                        <HeaderItem domain="/FindVets/" query={this.props.location.search} type="rating"/>
                    </PageHeader>
                    <br />
                    <br />
                    <div className="row">
                        {vetItems}
                    </div>
                </div>
                <br />
                <PageBar domain="/FindVets/" pageNum={pageNum} totalPages={this.props.total_page} query={this.props.location.search}/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
  vets: state.vets.vet_items,
  total_page: state.vets.vet_last_page,
  current_page: state.vets.vet_current_page
});

export default connect(mapStateToProps, { fetchData })(vetList)