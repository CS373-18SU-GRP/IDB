#!/usr/bin/env python3

import pprint

from elasticsearch import Elasticsearch

# client = Elasticsearch('localhost:9200')
client = Elasticsearch(
    'https://search-connectpetstome-duzal535fd4ilkkjotehb54qxe.us-east-1.es.amazonaws.com')
pp = pprint.PrettyPrinter(indent=1)
PAGINATION_SIZE = 3


def construct_query(search_phrase=''):
    query = {
        "query": {
            "match": {
                "_all": search_phrase
            }
        },
        "highlight": {
            "fields": {
                "_all": {}
            }
        }
    }

    return query


def pet_search(q, page=None, size=None, es=None):
    """
    input:  q: a search phrase from user
            page: page number (assuming the first page is 1, not 0)

    since from_ is the number of results to skip, so if page=1, then need to page -=1
    pagenation size: default 3

    return: list of dicts of pets ordered by Elasticsearch score (match with search_phrase)
            dict: include highlited texts (represents which parts are matched with search_phrase)
    """
    # this is for testing, we can set es client to test one
    if es == None:
        es = client

    if size == None:
        size = PAGINATION_SIZE

    if page == None:
        page = 1
    page -= 1
    query = construct_query(q)
    # pagination
    return es.search(index='connectpetstome_pet', body=query, from_=size * page, size=size)


def vet_search(q, page=None, size=None, es=None):
    """
    input:  q: a search phrase from user
            page: page number (assuming the first page is 1, not 0)

    since from_ is the number of results to skip, so if page=1, then need to page -=1
    pagenation size: default 3

    return: list of dicts of vets ordered by Elasticsearch score (match with search_phrase)
            dict: include highlited texts (represents which parts are matched with search_phrase)
    """
    # this is for testing, we can set es client to test one
    if es == None:
        es = client

    if size == None:
        size = PAGINATION_SIZE

    if page == None:
        page = 1
    page -= 1
    query = construct_query(q)
    # pagination
    return es.search(index='connectpetstome_vet', body=query, from_=size * page, size=size)


def shelter_search(q, page=None, size=None, es=None):
    """
    input:  q: a search phrase from user
            page: page number (assuming the first page is 1, not 0)

    since from_ is the number of results to skip, so if page=1, then need to page -=1
    pagenation size: default 3

    return: list of dicts of shelters ordered by Elasticsearch score (match with search_phrase)
            dict: include highlited texts (represents which parts are matched with search_phrase)
    """
    # this is for testing, we can set es client to test one
    if es == None:
        es = client

    if size == None:
        size = PAGINATION_SIZE

    if page == None:
        page = 1
    page -= 1
    query = construct_query(q)
    # pagination
    return es.search(index='connectpetstome_shelter', body=query, from_=size * page, size=size)


"""
fuzzy search
catch mis spelling
Use edit distance 1
"""
# query = {
#   "query": {
#     "fuzzy": {
#       "_all": {
#         "value": "austins",
#         "fuzziness": 1
#       }
#     }
#   }
# }

"""
multi_field search

specify which fields to search
we can boost specific field by adding "^2"
"""
# query = {
#   "query":{
#     "multi_match": {
#         "query":  "Thanks yoshi",
#         "fields": [ "description", "name"]
#     }
#   },
#   "highlight": {
#     "fields": {
#       "description": {},
#       "name": {}
#     }
#   }
# }
