from elasticsearch import Elasticsearch

#client = Elasticsearch('localhost:9200')
client = Elasticsearch('https://search-connectpetstome-duzal535fd4ilkkjotehb54qxe.us-east-1.es.amazonaws.com')
import pprint
pp = pprint.PrettyPrinter(indent=1)

def convert_sex(sex):
    if sex == 'M':
        return 'male'
    else:
        return 'female'

def convert_size(size):
    if size == 'S':
        return 'S small'
    elif size == 'M':
        return 'M medium'
    elif size == 'L':
        return 'L large'
    else:
        return 'XL extra large'

def pet_data_migration(data, es=None):
    if es == None:
        es = client
    pet = data[0]
    city = data[1]
    state = data[2]
    if state in states:
        state = state + ' ' + states[state]
    temperament = data[3]
    detail = data[4]
    doc = {
            'type': 'pet',
            'name': pet.name,
            'breed': pet.breed,
            'sex': convert_sex(pet.sex),
            'animal': pet.animal,
            'age': pet.age,
            'size': convert_size(pet.size),
            'description': pet.description,
            'city': city,
            'state': state,
            'temperament': temperament,
            'detail': detail,
            'image_url': pet.image_url,
        }
    
    #return doc
    es.create(index='connectpetstome_pet', doc_type='pet', body=doc, id=pet.id)

def vet_data_migration(vet):
    state = vet.state
    if vet.state in states:
        state = vet.state + ' ' + states[vet.state]

    if vet.rating == None or vet.rating < 3.5:
        rating = 'normal'
    elif vet.rating >= 4.5 and vet.rating <= 5.0:
        rating = 'best'
    else:
        rating = 'good'

    doc = {
            'type': 'vet veterinarian veterinary',
            'name': vet.name,
            'address1': vet.address1,
            'address2': vet.address2,
            'address3': vet.address3,
            'city': vet.city,
            'state': state,
            'country': vet.country,
            'zip_code': vet.zip_code,
            'display_phone': vet.phone,
            'rating': rating,
            'img_url': vet.image_url
        }
    
    #return doc
    client.create(index='connectpetstome_vet', doc_type='vet', body=doc, id=vet.id)

def shelter_data_migration(shelter):
    state = shelter.state
    if shelter.state in states:
        state = shelter.state + ' ' + states[shelter.state]
        
    doc = {
            'type': 'animal shelter',
            'name': shelter.name,
            'address1': shelter.address1,
            'address2': shelter.address2,
            'city': shelter.city,
            'state': state,
            'zip': shelter.zip,
            'phone': shelter.phone,
            'img_url': shelter.img_url
        }
    
    #return doc
    client.create(index='connectpetstome_shelter', doc_type='shelter', body=doc, id=shelter.id)


states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}