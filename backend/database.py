# from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
# from sqlalchemy.ext.declarative import declarative_base

from math import atan2, cos, radians, sin, sqrt

import geopy.distance
from flask import Flask
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

import getPetsUtil
from Elasticsearch.data_migration_util import *

db = SQLAlchemy()
ma = Marshmallow()

close_vet_shelter = db.Table(
    'close_vet_shelter',
    db.Column('vet_id', db.Unicode(500), db.ForeignKey('vet.id')),
    db.Column('shelter_id', db.Unicode(500), db.ForeignKey('shelter.id'))
)


class Pet(db.Model):
    __tablename__ = 'pet'
    id = db.Column('id', db.Unicode(500), primary_key=True)
    name = db.Column('name', db.String(500))
    breed = db.Column('breed', db.String(500))
    sex = db.Column('sex', db.String(500))
    animal = db.Column('animal', db.String(500))
    age = db.Column('age', db.String(500))
    size = db.Column('size', db.String(500))
    description = db.Column('description', db.String(10000))
    image_url = db.Column('image_url', db.String(500))
    # https://www.programcreek.com/python/example/78972/sqlalchemy.PickleType
    all_images = db.Column('all_images', db.PickleType())

    shelter_id = db.Column(db.Unicode(500), db.ForeignKey('shelter.id'))
    vet_id = db.Column(db.Unicode(500), db.ForeignKey('vet.id'))


class Shelter(db.Model):
    __tablename__ = 'shelter'

    id = db.Column('id', db.Unicode(500), primary_key=True)
    name = db.Column('name', db.String(500))
    # country = db.Column('country', db.String(500))
    state = db.Column('state', db.String(500))
    city = db.Column('city', db.String(500))
    address1 = db.Column('address1', db.String(500))
    address2 = db.Column('address2', db.String(500))
    zip = db.Column('zip', db.String(500))
    phone = db.Column('phone', db.String(500))
    longitude = db.Column('longitude', db.Float)
    latitude = db.Column('latitude', db.Float)
    email = db.Column('email', db.String(500))
    img_url = db.Column('img_url', db.String(500))

    pets = db.relationship('Pet', backref='shelter')
    close_vets = db.relationship(
        'Vet',
        secondary=close_vet_shelter,
        lazy='dynamic',
        backref=db.backref(
            'close_shelters',
            lazy='dynamic'))


class Vet(db.Model):
    __tablename__ = 'vet'
    id = db.Column('id', db.Unicode(500), primary_key=True)
    name = db.Column('name', db.String(500))
    image_url = db.Column('image_url', db.String(500))
    address1 = db.Column('address1', db.String(500))
    address2 = db.Column('address2', db.String(500))
    address3 = db.Column('address3', db.String(500))
    city = db.Column('city', db.String(500))
    state = db.Column('state', db.String(500))
    country = db.Column('country', db.String(500))
    zip_code = db.Column('zip_code', db.String(500))
    display_address = db.Column('display_address', db.String(500))
    phone = db.Column('phone', db.String(500))
    display_phone = db.Column('display_phone', db.String(500))
    latitude = db.Column('latitude', db.Float)
    longitude = db.Column('longitude', db.Float)
    rating = db.Column('rating', db.Float)
    review_count = db.Column('review_count', db.Integer)
    hours = db.Column('business_hours', db.PickleType())
    website_url = db.Column(
        'website_url',
        db.String(500))  # cannot do this with yelp
    # This doesnt work unless you do another request for each company
    all_images = db.Column('all_images', db.PickleType())

    pets = db.relationship('Pet', backref='vet')


class Breed(db.Model):
    __tablename__ = 'breed'
    id = db.Column('id', db.Unicode(500), primary_key=True)
    temperament = db.Column('temperament', db.String(5000))
    detail = db.Column('detail', db.String(5000))


class PetSchema(ma.ModelSchema):

    class Meta:
        model = Pet


class ShelterSchema(ma.ModelSchema):

    class Meta:
        model = Shelter


class VetSchema(ma.ModelSchema):

    class Meta:
        model = Vet


class BreedSchema(ma.ModelSchema):

    class Meta:
        model = Breed


def es_create_index(app):
    """
    create index for Pet, Vet, Shelter in Elasticsearch
    """
    db.init_app(app)
    create_pet_index()
    create_vet_index()
    create_shelter_index()


def es_data_migration(app):
    """
    migrate data from mySQL to Elasticsearch
    """
    db.init_app(app)

    # insert pet data into elasticsearch
    pets = db.session.query(Pet, Shelter.city, Shelter.state).\
        join(Shelter, Pet.shelter_id == Shelter.id).all()
    for i in pets:
        if i[0].breed:
            breed = Breed.query.get(i[0].breed)
            if breed:
                i = (*i, breed.temperament, breed.detail)
            else:
                i = (*i, '', '')
        else:
            i = (*i, '', '')

        pet_data_migration(i)
        print(i)

    # insert shelter data into elasticsearch
    shelters = Shelter.query.all()
    for i in shelters:
        shelter_data_migration(i)
        print(i)

    # insert vet data into elasticsearch
    vets = Vet.query.all()
    for i in vets:
        vet_data_migration(i)
        print(i)


def create_table(app):
    db.init_app(app)
    ma.init_app(app)
    # db = SQLAlchemy(app)
    # ma = Marshmallow(app)
    db.create_all()


def count_total_zip(filename):
    """
    count total num of zip in txt
    return total num of zip
    """
    total_zips = 0
    with open(filename, "r") as f:
        for line in f:
            if line[0] != '#':
                total_zips += 1
    return total_zips


def insert_vet(zipcode):
    """
    call getVetByLocation(zipcode), this will call
    the petfinder API and return SQLAlchemy Vet Classes
    Then, insert them into database
    """
    vList = set(getPetsUtil.getVetsByLocation(zipcode))
    for v in vList:
        if not db.session.query(Vet).filter(Vet.id == v.id).scalar():
            db.session.add(v)


def insert_shelter(petList):
    """
    input:  list of pet, get shelterId from pet class

    call getShelterByIdFromPetFinder(shelterId), this will call
    the petfinder API and return SQLAlchemy Shelter Classes
    Then, insert them into database

    return: list of pets whose shelterId could not find a shelter
    """
    deletePetList = set()
    shelterList = []
    for pet in petList:
        if pet:
            shelterId = pet.shelter_id
            if shelterId not in shelterList:
                shelter = getPetsUtil.getShelterByIdFromPetFinder(
                    shelterId)
                if shelter is None:
                    deletePetList.add(pet)
                else:
                    shelterList.append(shelterId)
                    if not db.session.query(Shelter).filter(
                            Shelter.id == shelter.id).scalar():
                        db.session.add(shelter)
    return deletePetList


def insert_close_vet_shelter_bridgeTable():
    """
    Currently, find vets in the same state where a shelter locates
    """
    vets = Vet.query.all()
    for v in vets:
        close_shelters = Shelter.query.filter_by(state=v.state)
        for sh in close_shelters:
            sh.close_vets.append(v)


def insert_data(app, filename='smallzip.txt'):
    """
    input : Either 'smallzip.txt' or 'middlezip.txt' or 'largezip.txt' or 'hugezip.txt'
    insert Pet, Shelter, Vet into database
    change filename -> amount of data to be inserted change
    """
    db.init_app(app)
    totalzips = count_total_zip(filename)
    count = 1

    with open(filename, 'r') as file:
        for zipcode in file:
            if zipcode[0] != '#':
                # ADD VETS
                insert_vet(zipcode)

                animal_list1 = {
                    'bird': 7,
                    'dog': 30,
                    'cat': 24
                }
                animal_list2 = {
                    'barnyard': 2,
                    'smallfurry': 4,
                    'reptile': 4,
                    'horse': 2,
                    'rabbit': 4
                }
                petList = set()
                for animal, num in animal_list1.items():
                    petList.update(
                        set(getPetsUtil.getPetsFromPetFinder(animal, num, zipcode)))
                if filename == 'hugezip.txt':
                    for animal, num in animal_list1.items():
                        petList.update(
                            set(getPetsUtil.getPetsFromPetFinder(animal, num, zipcode)))

                # ADD SHELTER
                deletePetList = insert_shelter(petList)
                petList = petList.difference(deletePetList)

                # FILLING THE BRIDGE TABLE BETWEEN VETS AND SHELTERS
                insert_close_vet_shelter_bridgeTable()

                # ADD PETS
                for p in petList:

                    # find a closest vet from the shelter where this pet (p)
                    # belongs
                    parent_shelter = Shelter.query.get(p.shelter_id)
                    closest_vet = None
                    if parent_shelter:
                        max_dist = 0
                        for v in parent_shelter.close_vets:
                            distance = calculate_distance(
                                parent_shelter.latitude, parent_shelter.longitude, v.latitude, v.longitude)
                            if closest_vet is None or max_dist < distance:
                                closest_vet = v
                                max_dist = distance

                    if not db.session.query(Pet).filter(Pet.id == p.id).scalar():
                        if closest_vet:
                            p.vet_id = closest_vet.id
                        db.session.add(p)

                db.session.commit()
                count += 1


def calculate_distance(lat_1, log_1, lat_2, log_2):
    """
    calculate distance between (latitude1, logitude1) and
    (latitude2, logitude2) in miles
    """
    coords_1 = (lat_1, log_1)
    coords_2 = (lat_2, log_2)

    return geopy.distance.geodesic(coords_1, coords_2).miles
